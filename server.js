const express = require("express");

// Mongoose is a package tha allows us to create Schemas to Model our data structures and to manipulate our database using different access methods
const mongoose = require("mongoose");


const port = 3001;
const app = express();


// [SECTION] MongoDB connection
	// Syntax: 
		//  mongoose.connect("mongoDBconnectionString", {options to avoid errors in our connection})
	mongoose.connect("mongodb+srv://admin:admin@batch245-sanroque.9babxkw.mongodb.net/s35-discussion?retryWrites=true&w=majority",
			{
				// Allows us to avoid any current and future errors while connecting to MongoDB
				useNewUrlParser: true,
				useUnifiedTopology: true
		})

	let db = mongoose.connection;

	// for error handling in connecting
	db.on("error", console.error.bind(console, "Connection Error"));

	// this will be triggered if the connection is successful
	db.once("open", () => console.log("We're connected to the cloud database!"));

	// Mongoose Schemas
		// Schemas determine the structure of the documents to be written in the database
		// Schemas act as the blueprint to our data
		// Syntax:
			// const schemaName = new mongoose.Schema({keyValuePairs});


	//taskSchema - it contains two properties: name & status
	//required is used to specify that a field must be empty

	//default is used if a field value is not supplied
	const tasksSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, "Task name is required!"]
		},
		status: {
			type: String,
			default: "pending"
		}
	});

//*ACTIVITY - usersSchema
	const usersSchema = new mongoose.Schema({
		username: {
			type: String,
			required: [true, "User name is required!"]
		},
		password: {
			type: String,
			default: "n/a"
		}
	});



	//[SECTION] Models
		// Uses schema to create/instatiate doxuments/objects that follows our schema structure

		// the variable/object that will be created can be used to run commands with our database

		// Syntax:
			//const variableName = mongoose.model("collectionName", schemaName);


	const Task = mongoose.model("Task", tasksSchema);

//*ACTIVITY - mongoose.model for users
	const User = mongoose.model("User", usersSchema);


	//middlewares
		app.use(express.json()); //allows app to read json data
		app.use(express.urlencoded({extended: true})); // it will allow our app to read data from forms.


	//[SECTION] Routing
		// Create/add new task
			//1. Check if the task is existing.
					//if task already exist in the database, we will return a message "the task is already existing!"
					//id the task doesnt exist in the database, we will add it in the database

	app.post("/tasks", (request, response) => {
		let input = request.body
		console.log(input.status);

		if(input.status === undefined){
			Task.findOne({name: input.name}, (error, result) => {
				console.log(result);
				if(result !== null){
					return response.send(`The task is already existing!`);
				} else{
					let newTask = new Task({
						name: input.name
					});

					//save() method will save the object in the collection that the object instatiated
					newTask.save((saveError, saveTask) => {
						if(saveError){
							return console.log(saveError);
						} else{
							return response.send("New Task created!")
						}
					})
				}	
			})
		} else{
			Task.findOne({name: input.name}, (error, result) => {
				console.log(result);
				if(result !== null){
					return response.send(`The task is already existing!`);
				} else{
					let newTask = new Task({
						name: input.name,
						status: input.status
					})
					newTask.save((saveError, saveTask) => {
						if(saveError){
							return console.log(saveError);
						} else{
							return response.send("New Task created!")
						}
					})
				}			
			}) 
		}
	}) 


	//[SECTION] Retrieving the all the tasks
	app.get("/tasks", (request, response) => {
		// empty criteria {} to find all
		Task.find({}, (error, result) => {
			if(error){
				console.log(error);
			} else{
				return response.send(result);
			}
		})
	})




//*Activity
//signup
app.post("/signup", (request, response) => {
	let user = request.body
	console.log(user.password);

	if(user.password === undefined){
		User.findOne({username: user.username}, (error, result) => {
			console.log(result);
			if(result !== null){
				return response.send(`That username already exist!`);
			} else{
				let newUser = new User({
					username: user.username
				});

				//save() method will save the object in the collection that the object instatiated
				newUser.save((saveError, saveUser) => {
					if(saveError){
						return console.log(saveError);
					} else{
						return response.send("New User created!")
					}
				})
			}	
		})
	} else{
		User.findOne({username: user.username}, (error, result) => {
			console.log(result);
			if(result !== null){
				return response.send(`That username already exist!!`);
			} else{
				let newUser = new User({
					username: user.username,
					password: user.password
				})
				newUser.save((saveError, saveUser) => {
					if(saveError){
						return console.log(saveError);
					} else{
						return response.send("New User created!")
					}
				})
			}			
		}) 
	}
}) 
	
// retrieve all users
app.get("/users", (request, response) => {
	User.find({}, (error, result) => {
		if(error){
			console.log(error);
		} else{
			return response.send(result);
		}
	})
})



// app.listen
app.listen(port, ()=> console.log(`Server is running at ${port}!`));